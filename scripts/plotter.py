#!/usr/bin/env python
from matplotlib import pyplot as plt
import rospy
from panda_mqtt_connector.msg import StampedVelocity

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

new_velocity = []

fig, ax = plt.subplots()
xdata_velocity, ydata_velocity = [], []
ln, = plt.plot([], [], 'ro')

def callback_velocity(msg):
    global new_velocity
    new_velocity += [msg]

def init_velocity():
    ax.set_xlim(0, 150)
    ax.set_ylim(-1.2, 3)
    return ln,

def update_velocity(arg):
    print "update", arg
    global new_velocity
    for msg in new_velocity:
        xdata_velocity.append(msg.header.stamp.to_sec())
        ydata_velocity.append(msg.velocity)
    ln.set_data(xdata, ydata)
    new_velocity = []
    return ln,

if __name__ == '__main__':

    rospy.init_node("plotter")
    rospy.Subscriber("panda_mqtt_connector/velocity", StampedVelocity, callback_velocity)
    # plt.ion()
    ani = FuncAnimation(fig, update_velocity, None, init_func=init_velocity, blit=True)
    plt.show()


    rospy.spin()

