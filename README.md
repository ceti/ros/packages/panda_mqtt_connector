# panda_mqtt_connector


## Installing Eclipse Paho MQTT-Client for C++

The build process currently supports a number of Unix and Linux flavors. The build process requires the following tools:

  * CMake v3.5 or newer
  * GCC v4.8 or newer or Clang v3.9 or newer
  * GNU Make

On Debian based systems this would mean that the following packages have to be installed:

```
$ sudo apt-get install build-essential gcc make cmake cmake-gui cmake-curses-gui
$ sudo apt-get install libssl-dev
$ sudo apt-get install doxygen graphviz
$ sudo apt-get install libcppunit-dev
```

## How to start it

### Installation

1. Install ROS
2. Clone the Catkin Workspace (via the Meta-Project)
3. Install Eclipse Paho (see previous section)
4. Build it via catkin build

### Start the System

* Production-Mode: roslaunch panda_mqtt_connector simulation_rosrag.launch
    - the default mqtt server address is defined in config/mqtt.yaml
    - it must follow the notation [as specified](https://www.eclipse.org/paho/files/mqttdoc/MQTTClient/html/_m_q_t_t_client_8h.html#a9a0518d9ca924d12c1329dbe3de5f2b6) by the employed [Eclipse Paho](https://www.eclipse.org/paho/) mqtt library:
      > A null-terminated string specifying the server to which the client will connect.
        It takes the form protocol://host:port. Currently, protocol must be tcp or ssl.
        For host, you can specify either an IP address or a host name. For instance,
        to connect to a server running on the local machines with the default MQTT port,
        specify tcp://localhost:1883. 
    - Please note that SSL is currently not supported.
    - The default server address can be changed by adding the parameter `mqtt_server` to the launch file:
      `roslaunch panda_mqtt_connector simulation_rosrag.launch mqtt_server:="tcp://<address>:<port>"`
* Test-Mode: roslaunch panda_mqtt_connector simulation_rosrag_test.launch 
	* Starts an additional node which listens to MQTT channels for testing
	* Channel 1: configtest -> manipulates messages (switch on/off the message parts) and the publish-rate
	* Channel 2: modetest -> changes planning mode to "fluid"
	* Channel 3: trajectorytest -> updates the trajectory
	* Message is arbitrary, content is important

## Provided MQTT-Interfaces

### Configuration of the data-stream (ROS -> RAG)

#### General

* Channel: dataconfig
* Protobuff-message format defined in https://git-st.inf.tu-dresden.de/ceti/ros/panda_mqtt_connector/-/blob/development/morse/proto/dataconfig.proto
* Defines the data submitted from ros to mqtt
* Changes submitted data starting from the next data-package retreived from gazebo

#### Parameters

* Parameter "data_publish_rate" means in which intervals messages are pulled out of gazebo's data stream
* data_enable_position, true -> send joint positions
* data_enable_orientation, true -> send joint orientations
* data_enable_twist_linear, true -> send linear twists of joints
* data_enable_twist_angular, true -> send angular twists of joints

### Configuration of the simulated robot

#### General

* Channel: robotconfig
* allows to reconfigure velocity and planning-mode of the simulated robot
* Protobuff-message format defined in https://git-st.inf.tu-dresden.de/ceti/ros/panda_mqtt_connector/-/blob/development/morse/proto/robotconfig.proto

#### Parameters

* robot_speed_factor --> defines how fast the robot moves starting from the next trajectory-point
	* is a factor multiplied to the speed computed by the trajectory-planning algorithm
	* Range: 0,1 to 1,0 (recommed are values > 0.3)
* robot_planning_mode (enum)
	* cartesian_path ( 1 in enum ) = use cartesian path based planning 
	* fluid_path ( 0 in enum ) = use "normal" planning methods
* loopTrajectory (boolean)
	* true -> always loop current trajectory (infinite)
	* false -> execute next trajectory only once and terminate (remark better reset trajectory before this by configuring an empty trajectory)

### Datastream format

#### General

* Data for every link has its own mqtt-channel (defined in section bellow)
* Protobuff-message format defined in https://git-st.inf.tu-dresden.de/ceti/ros/panda_mqtt_connector/-/blob/development/morse/proto/linkstate.proto

#### Channels

* panda_ground: ground-plate
* panda_link_0: link_0 of the panda
* panda_link_1: link_1 of the panda
* panda_link_2: link_2 of the panda
* panda_link_3: link_3 of the panda
* panda_link_4: link_4 of the panda
* panda_link_5: link_5 of the panda
* panda_link_6: link_6 of the panda
* panda_link_7: link_7 of the panda (end effector)
* panda_link_8: link_8 of the panda (left finger)
* panda_link_9: link_9 of the panda (right finger)

### Configuration of the Trajectory

#### General

* Configures the waypoints of the robot (at the beginning the robot uses a default trajectory)
* Configures 3D-Position of the End-effector

#### Parameters

* MQTT-channel: trajectoryconfig
* Protobuff-message format defined in https://git-st.inf.tu-dresden.de/ceti/ros/panda_mqtt_connector/-/blob/development/morse/proto/trajectory.proto

## Changelog

   #### Safetyzones

   ![Zone](assets/box.png?raw=true "Safetyzone")

   Integrates safetyzones via the SafetyAwarePlanner. 
   start-command: 
   ...
   roslaunch panda_mqtt_connector simulation.launch
   ...

   To see through the boxes one have to set the alpha of the planning an environment (see picture).

   #### _MoveIt!_ constraint-aware planning

   An example of one can add constraints to the planning and simulate it. start-command: 
   start-command:
   ...
   roslaunch panda_mqtt_connector simulation_constraint.launch
   ...

   This repository was extended with a ROS node that communicates with the _MoveIt!_ Planning Scene API. It makes sure that the motion planning pipeline avoids collision objects in the environment specified by the user in a separate directory (`~/.panda_mqtt_connector`) as _json_ files.

   [Publishing a box at Panda's hand in _Gazebo_](https://erdalpekel.de/?p=123)

   This repository was extended with a node that publishes a simple box object in the _Gazebo_ simulation at the hand of the robot. The position of this box will get updated as soon as the robot moves.

   [Visual Studio Code Remote Docker](https://erdalpekel.de/?p=123)

   I have added configuration files and additional setup scripts for developing and using this ROS package within a *Docker* container. Currently user interfaces for Gazebo and RViz are not supported.

   [Position based trajectory execution](https://erdalpekel.de/?p=285)

   The joint specifications in Gazebo were changed from an effort interface to position based interface. Furthermore, the PID controller was substituted with the simple gazebo internal position based control mechanism for a more stable movement profile of the robot. A custom joint position based controller was implemented in order to set the initial joint states of the robot to a valid configuration.

   [Automatic robot state initialization](https://erdalpekel.de/?p=314)

   A separate ROS node was implemented that starts a custom joint position controller and initializes the robot with a specific configuration. It switches back to the default controllers after the robot reaches the desired state.

![Panda state initialization in Gazebo](assets/robot-state-initializer.gif?raw=true "Panda state initialization in Gazebo")
