#include "mqtt/client.h"

#include "ros/ros.h"

#include "util/MqttUtil.h"

#include "panda_mqtt_connector/Trajectory.h"
#include "panda_mqtt_connector/Waypoint.h"
#include "panda_mqtt_connector/MqttMessage.h"
#include "std_msgs/Float64.h"

#include "config.pb.h"
#include "trajectory.pb.h"
#include "util/TrajectoryUtil.h"

const std::string CLIENT_ID{"mqtt_to_ros"};

const std::string ROBOT_CONFIG{"robotconfig"};
const std::string TRAJECTORY_CONFIG{"trajectory"};

MqttUtil *mqttUtil = nullptr;

void handleRobotConfig(const config::RobotConfig &robotConfig, const ros::Publisher &velocityPublisher) {
  ROS_INFO_STREAM("Received new target-speed: " << robotConfig.speed());
  std_msgs::Float64 velocity;
  velocity.data = robotConfig.speed();
  velocityPublisher.publish(velocity);
}

void handleNewTrajectory(const plan::Trajectory &protoTrajectory, const ros::Publisher &trajectoryPublisher) {
  ROS_INFO_STREAM("Received new trajectory  with " << protoTrajectory.pose().size() << " points.");
  panda_mqtt_connector::Trajectory trajectory;

  for (const auto &protoPose : protoTrajectory.pose()) {
    panda_mqtt_connector::Waypoint waypoint;

    tf2::Quaternion orientation{protoPose.orientation().x(), protoPose.orientation().y(), protoPose.orientation().z(), protoPose.orientation().w()};
    orientation = orientation.normalize();
    waypoint.pose.orientation.w = orientation.w();
    waypoint.pose.orientation.x = orientation.x();
    waypoint.pose.orientation.y = orientation.y();
    waypoint.pose.orientation.z = orientation.z();

    waypoint.pose.position.x = protoPose.position().x();
    waypoint.pose.position.y = protoPose.position().y();
    waypoint.pose.position.z = protoPose.position().z();

    switch (protoPose.mode()) {
      case plan::Trajectory_PlanningMode_FLUID:
        waypoint.mode = TrajectoryUtil::FLUID_PATH;
        break;
      case plan::Trajectory_PlanningMode_CARTESIAN:
        waypoint.mode = TrajectoryUtil::CARTESIAN_PATH;
        break;
      default:
        ROS_WARN("Received pose has invalid mode!");
    }

    trajectory.waypoints.push_back(waypoint);
  }

  trajectory.loop = protoTrajectory.loop();

  trajectoryPublisher.publish(trajectory);

}

void receiveMqttMessages(const ros::NodeHandle &n, const ros::Publisher &trajectoryPublisher, const ros::Publisher &velocityPublisher, const ros::Publisher &mqttMessagePublisher) {
  if (mqttUtil->ensureConnection()) {
    mqtt::const_message_ptr msg;
    if (mqttUtil->getClient().try_consume_message_for(&msg, std::chrono::milliseconds(500))) {
      if (msg->get_topic() == ROBOT_CONFIG) {
        const std::string rc_payload = msg->get_payload_str();
        config::RobotConfig robotConfig;
        robotConfig.ParseFromString(rc_payload);
        handleRobotConfig(robotConfig, velocityPublisher);
      } else if (msg->get_topic() == TRAJECTORY_CONFIG) {
        const std::string tc_payload = msg->get_payload_str();
        plan::Trajectory trajectoryConfig;
        trajectoryConfig.ParseFromString(tc_payload);
        handleNewTrajectory(trajectoryConfig, trajectoryPublisher);
      } else {
        ROS_INFO_STREAM("Retrieved new MQTT message on topic " << msg->get_topic());
        panda_mqtt_connector::MqttMessage mqttMessage;
        mqttMessage.topic = msg->get_topic();
        for (auto b : msg->get_payload()) {
          mqttMessage.content.push_back(b);
        }
        mqttMessagePublisher.publish(mqttMessage);
      }
    }
  } else {
    ROS_ERROR_STREAM_NAMED("MqttToRosNode", "Not connected! Unable to listen to messages.");
  }
}

int main(int argc, char *argv[]) {
  ros::init(argc, argv, "mqtt_listener");
  ros::NodeHandle n("panda_mqtt_connector");

  ros::Publisher trajectoryPublisher = n.advertise<panda_mqtt_connector::Trajectory>("trajectory_update", 1000);
  ros::Publisher maxVelocityPublisher = n.advertise<std_msgs::Float64>("max_velocity", 1000);
  ros::Publisher mqttMessagePublisher = n.advertise<panda_mqtt_connector::MqttMessage>("mqtt_message", 1000);

  std::string server;
  if (!n.getParam("server", server)) {
    ROS_ERROR_STREAM("Could not get string value for " << n.getNamespace() << "/server from param server");
    return -1;
  }

  mqttUtil = new MqttUtil(CLIENT_ID, server);

  std::map<std::string, std::string> topics;
  if (!n.getParam("topics", topics)) {
    ROS_ERROR_STREAM("Could not get string value for " << n.getNamespace() << "/topics from param server");
  }

  for (const auto &topic : topics) {
    ROS_INFO_STREAM("Listening to MQTT topic " << topic.second);
    mqttUtil->addTopic(topic.second);
  }

  if (!mqttUtil->connect()) {
    ROS_ERROR_STREAM("Unable to connect to MQTT server. Exiting...");
    return -1;
  }

  while (ros::ok()) {
    receiveMqttMessages(n, trajectoryPublisher, maxVelocityPublisher, mqttMessagePublisher);
    ros::spinOnce();
  }

  return 0;
}
