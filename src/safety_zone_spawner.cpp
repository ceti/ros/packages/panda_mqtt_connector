/*! \file safety_zone_spawner.cpp
    \brief ROS node main file that spawns safety zones in gazebo.

    \author Johannes Mey
    \date 28.06.20
*/

#include <ros/ros.h>
#include <shape_msgs/SolidPrimitive.h>
#include <geometry_msgs/Pose.h>
#include <GazeboZoneSpawner.h>

/**
 * The main method of the ROS node
 *
 * It does not requite command-line parameters, but expects the ROS parameter `/safety_zone_spawner/zone_size` and
 * `/safety_zone_spawner/zones` to be set.
 *
 *  - `/safety_zone_spawner/zone_size: double`
 *
 *    the edge size of a safety zone cube to be displayed in *m*.
 *  - `/safety_zone_spawner/zones: std::vector<std::string>`
 *
 *    a list of safety zone cube integer coordinates. The coordinates are encoded in a string and separated by spaces
 *    and have an `x`, a `y`, and and optionally a `z` coordinate. Examples are `"3 0"` or `"-5 2 4"`. The coordinates
 *    are interpreted as follows: the cube at position "0 0 0" is centered at the origin of the coordinate system in
 *    `x` and `y` direction, but is located *on top of the x-y-plane*. All other node coordinates are defined such that
 *    they completely fill the space. This means that the actual coordinates (in *m*) can be computed using the ones
 *    given here and the `zone_size`.
 */
int main(int argc, char **argv) {

  // setup this ros-node
  ros::init(argc, argv, "safety_zone_spawner");
  ros::NodeHandle node_handle("panda_mqtt_connector");

  ros::AsyncSpinner spinner(1);
  spinner.start();

  // wait for gazebo
  // TODO this should actually wait until gazebo has started
  ros::Duration(2.0).sleep();

  // get size
  double size = 0.5;
  if (!node_handle.getParam("zone_size", size)) {
    ROS_WARN_STREAM("Could not get string value for " << node_handle.getNamespace() << "/size from param server, defaulting to " << size);
  }

  shape_msgs::SolidPrimitive box;
  box.type = shape_msgs::SolidPrimitive::BOX;
  box.dimensions = {size, size, size};

  // get list of zone voxels
  std::vector<std::string> voxels;
  if (!node_handle.getParam("zones", voxels)) {
    ROS_WARN_STREAM("Could not get string value for " << node_handle.getNamespace() << "/zones from param server. Not adding any zones.");
  }

  for (const std::string &voxel: voxels) {
    geometry_msgs::Pose pose;

    std::stringstream stream(voxel);
    int x, y, z = 0;
    stream >> x >> y;
    if (stream) {
      stream >> z; // the z value is 0 by default
    }

    pose.position.x = x * size;
    pose.position.y = y * size;
    pose.position.z = z * size + size / 2;

    // the orientation is always the default
    pose.orientation.w = 1;

    GazeboZoneSpawner::spawnCollisionBox(box, pose);
  }

}