//
// Created by jm on 18.07.20.
//

#ifndef SRC_SAFETYZONEMONITOR_H
#define SRC_SAFETYZONEMONITOR_H

#include <geometry_msgs/Point.h>
#include <geometry_msgs/PoseStamped.h>

#include <panda_mqtt_connector/StampedInZone.h>
#include "ros/ros.h"


class SafetyZoneMonitor {

private:

  struct Zone {
    geometry_msgs::Point zone;
    double x_min;
    double y_min;
    double z_min;
    double x_max;
    double y_max;
    double z_max;

    Zone(geometry_msgs::Point zone, double zone_size);

    bool operator==(const Zone &rhs) const;

    bool operator!=(const Zone &rhs) const;

    bool operator<(const Zone &rhs) const;

    bool operator>(const Zone &rhs) const;

    bool operator<=(const Zone &rhs) const;

    bool operator>=(const Zone &rhs) const;

    bool contains(const geometry_msgs::Point &point) const;
  };

  std::map<Zone, std::map<std::string,bool>> zones;
  double zone_size;
  std::shared_ptr<ros::Publisher> inZonePublisher;

  bool in_zone{};
  unsigned int zone_changes{};
  unsigned int mode_changes{};
public:

  SafetyZoneMonitor(double zoneSize, ros::Publisher &publisher);

  void addZone(geometry_msgs::Point zone);

  void providerCallback(const geometry_msgs::PoseStamped &msg);

};


#endif //SRC_SAFETYZONEMONITOR_H
