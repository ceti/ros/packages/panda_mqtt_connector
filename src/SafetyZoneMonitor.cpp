//
// Created by jm on 18.07.20.
//

#include "SafetyZoneMonitor.h"

SafetyZoneMonitor::SafetyZoneMonitor(double zoneSize, ros::Publisher &publisher) : zone_size(zoneSize), inZonePublisher(&publisher) {}

void SafetyZoneMonitor::addZone(geometry_msgs::Point zone) {
  zones[Zone(zone, zone_size)] = std::map<std::string, bool>();
}

void SafetyZoneMonitor::providerCallback(const geometry_msgs::PoseStamped &msg) {
  static bool initialized;

  bool zoneChanged = false;

  bool inZoneNow = false;
  for (auto zone : zones) {
    bool containsNow = zone.first.contains(msg.pose.position);
    if (containsNow) {
      inZoneNow = true;
    }
    if (containsNow != zone.second[msg.header.frame_id]) {
      ++zone_changes;
      ROS_ERROR_STREAM("zone changed from " << zone.second[msg.header.frame_id] <<" to " << containsNow);
      zoneChanged = true;
      zones[zone.first][msg.header.frame_id] = containsNow;
      ROS_ERROR_STREAM("zone changed for " << msg.header.frame_id << " to " << zones[zone.first][msg.header.frame_id]);
    }
  }

  bool modeChanged = false;
  if (in_zone != inZoneNow) {

    if (in_zone) {
      // check for all other zones, too, if we left the zone with the current link
      in_zone = false;
      for (const auto& zone : zones) {
        for (const auto& link : zone.second) {
          if (link.second) {
            in_zone = true;
            break;
          }
        }
        if (in_zone) {
          break;
        }
      }
      if (!in_zone) {
        ROS_ERROR_STREAM("mode for link " << msg.header.frame_id << "changed to " << in_zone);
        ++mode_changes;
        modeChanged = true;
      }
    } else {
      in_zone = true;
      ++mode_changes;
      modeChanged = true;
      ROS_ERROR_STREAM("mode for link " << msg.header.frame_id << "changed to " << in_zone);
    }
  }


  if (zoneChanged || !initialized) {
//    ROS_ERROR_STREAM("publishing message.");
    initialized = true;
    panda_mqtt_connector::StampedInZone result;
    result.zone = in_zone;
    result.zone_changes = zone_changes;
    result.mode_changes = mode_changes;
    result.mode_changed = modeChanged;
    result.header.stamp = ros::Time::now();
    inZonePublisher->publish(result);
  }
}

SafetyZoneMonitor::Zone::Zone(geometry_msgs::Point zone, double zone_size) :
    zone(zone),
    x_min((zone.x - 0.5) * zone_size),
    y_min((zone.y - 0.5) * zone_size),
    z_min((zone.z) * zone_size),
    x_max((zone.x + 0.5) * zone_size),
    y_max((zone.y + 0.5) * zone_size),
    z_max((zone.z + 1.0) * zone_size) {}

bool SafetyZoneMonitor::Zone::contains(const geometry_msgs::Point &point) const {
  return point.x >= x_min && point.x <= x_max &&
         point.y >= y_min && point.y <= y_max &&
         point.z >= z_min && point.z <= z_max;
}

bool SafetyZoneMonitor::Zone::operator<(const SafetyZoneMonitor::Zone &rhs) const {
  if (x_min < rhs.x_min)
    return true;
  if (rhs.x_min < x_min)
    return false;
  if (y_min < rhs.y_min)
    return true;
  if (rhs.y_min < y_min)
    return false;
  if (z_min < rhs.z_min)
    return true;
  if (rhs.z_min < z_min)
    return false;
  if (x_max < rhs.x_max)
    return true;
  if (rhs.x_max < x_max)
    return false;
  if (y_max < rhs.y_max)
    return true;
  if (rhs.y_max < y_max)
    return false;
  return z_max < rhs.z_max;
}

bool SafetyZoneMonitor::Zone::operator>(const SafetyZoneMonitor::Zone &rhs) const {
  return rhs < *this;
}

bool SafetyZoneMonitor::Zone::operator<=(const SafetyZoneMonitor::Zone &rhs) const {
  return !(rhs < *this);
}

bool SafetyZoneMonitor::Zone::operator>=(const SafetyZoneMonitor::Zone &rhs) const {
  return !(*this < rhs);
}

bool SafetyZoneMonitor::Zone::operator==(const SafetyZoneMonitor::Zone &rhs) const {
  return x_min == rhs.x_min &&
         y_min == rhs.y_min &&
         z_min == rhs.z_min &&
         x_max == rhs.x_max &&
         y_max == rhs.y_max &&
         z_max == rhs.z_max;
}

bool SafetyZoneMonitor::Zone::operator!=(const SafetyZoneMonitor::Zone &rhs) const {
  return !(rhs == *this);
}
