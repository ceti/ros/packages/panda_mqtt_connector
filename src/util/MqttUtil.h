//
// Created by Johannes Mey on 01.06.20.
//

#ifndef PANDA_MQTT_CONNECTOR_MQTTUTIL_H
#define PANDA_MQTT_CONNECTOR_MQTTUTIL_H

#include <mqtt/async_client.h>
#include <mqtt/client.h>

const std::string DEFAULT_SERVER_ADDRESS{"tcp://localhost:1883"};

class MqttUtil {

private:
  mqtt::client client;
  mqtt::string_collection topics;

public:
  MqttUtil(std::string client_id, std::string server = DEFAULT_SERVER_ADDRESS);
  const mqtt::string_collection &getTopics() const;
  void addTopic(std::string topic);

  mqtt::client &getClient();

  bool connect();

  bool ensureConnection();
  virtual ~MqttUtil();
};

#endif // PANDA_MQTT_CONNECTOR_MQTTUTIL_H
