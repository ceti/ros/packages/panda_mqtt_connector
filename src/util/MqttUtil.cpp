//
// Created by Johannes Mey on 01.06.20.
//

#include "MqttUtil.h"

#include "ros/ros.h"

bool MqttUtil::connect() {
  try {
    if (!client.is_connected()) {
      ROS_INFO_STREAM_NAMED("MqttUtil", client.get_client_id() << ": Initializing MQTT");
      mqtt::connect_options connOpts;
      connOpts.set_keep_alive_interval(20);
      connOpts.set_connect_timeout(1);
      connOpts.set_clean_session(false);
      auto rsp = client.connect(connOpts);
      if (!rsp.is_session_present()) {
        // only subscribe if there are topics
        if (!topics.empty()) {
          ROS_INFO_STREAM_NAMED("MqttUtil", client.get_client_id() << ": Subscribing to " << topics.size() << " topics.");
          auto srsp = client.subscribe(topics);
        }
      }
      return true;
    } else {
      ROS_WARN_STREAM_NAMED("MqttUtil", client.get_client_id() << ": Client is already connected.");
      return true;
    }
  } catch (const mqtt::exception &e) {
    ROS_ERROR_STREAM_NAMED("MqttUtil", client.get_client_id() << ": Unable to connect to " << client.get_server_uri() << ".");
    return false;
  }
}

bool MqttUtil::ensureConnection() {
  constexpr int N_ATTEMPT = 30;

  if (!client.is_connected()) {
    ROS_WARN_STREAM_NAMED("MqttUtil", client.get_client_id() << ": Lost connection. Reconnecting.");
    for (int i = 0; i < N_ATTEMPT && !client.is_connected(); ++i) {
      try {
        auto rsp = client.reconnect();
        if (!rsp.is_session_present()) {
          client.subscribe(topics);
        }
        return true;
      } catch (const mqtt::exception &) { std::this_thread::sleep_for(std::chrono::seconds(1)); }
    }
    ROS_ERROR_STREAM_NAMED("MqttUtil", client.get_client_id() << ": Reconnection failed!");
    return false;
  }
  return true;
}

const mqtt::string_collection &MqttUtil::getTopics() const { return topics; }

void MqttUtil::addTopic(std::string topic) { topics.push_back(topic); }

MqttUtil::MqttUtil(std::string client_id, std::string server) : topics(), client(server, client_id) {}

MqttUtil::~MqttUtil() { client.disconnect(); }
mqtt::client &MqttUtil::getClient() { return client; }
