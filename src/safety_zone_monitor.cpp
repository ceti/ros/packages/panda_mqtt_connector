/*! \file safety_zone_spawner.cpp
    \brief ROS node main file that spawns safety zones in gazebo.

    \author Johannes Mey
    \date 28.06.20
*/

#include <ros/ros.h>
#include <shape_msgs/SolidPrimitive.h>
#include <geometry_msgs/Pose.h>
#include <panda_mqtt_connector/StampedInZone.h>
#include "SafetyZoneMonitor.h"

namespace safety_zone_monitor {
  std::vector<std::string> readTopicList(const ros::NodeHandle &n) {
    std::vector<std::string> topics;

    const std::vector<std::string> elementTypes{"/parts/", "/end_effectors/"};
    std::vector<std::string> parameterNames;
    n.getParamNames(parameterNames);
    std::set<std::string> groups{};
    for (const auto &elementType : elementTypes) {
      // get the groups
      for (const auto &param : parameterNames) {
        std::string prefix{n.getNamespace() + elementType};
        if (param.rfind(prefix, 0) == 0) {
          std::string rest{param.substr(prefix.size())};
          std::string element{rest.substr(0, rest.rfind('/'))};
          groups.insert(element);
        }
      }

      // get the elements in the group
      for (const auto &group: groups) {
        std::map<std::string, std::string> element_topics{};
        std::string key{n.getNamespace() + elementType + group};
        if (!n.getParam(key, element_topics)) {
          ROS_ERROR_STREAM("Unable to retrieve value for " << key);
        }
        for (const auto &pair : element_topics) {
          std::string mqttTopic{group + "/" + pair.first};
          topics.push_back(mqttTopic);
        }
      }
    }

    ROS_INFO_STREAM("Observing " << topics.size() << " Gazebo elements.");
    for (const auto &topic : topics) {
      ROS_INFO_STREAM("Observing gazebo element: " << topic);
    }
    return topics;
  }
}

/**
 * The main method of the ROS node
 *
 */
int main(int argc, char **argv) {

  // setup this ros-node
  ros::init(argc, argv, "safety_zone_monitor");
  ros::NodeHandle n("panda_mqtt_connector");

  // wait for gazebo
  // TODO this should actually wait until gazebo has started
  ros::Duration(2.0).sleep();

  // get size
  double size = 0.5;
  if (!n.getParam("zone_size", size)) {
    ROS_ERROR_STREAM("Could not get string value for " << n.getNamespace() << "/size from param server.");
    exit(-1);
  }

  ros::Publisher zoneStatePublisher = n.advertise<panda_mqtt_connector::StampedInZone>("robot_in_zone", 1000);

  SafetyZoneMonitor monitor{size, zoneStatePublisher};

  // get list of zone voxels
  std::vector<std::string> voxels;
  if (!n.getParam("zones", voxels)) {
    ROS_WARN_STREAM("Could not get string values for " << n.getNamespace() << "/zones from param server. Exiting.");
    exit(0);
  }

  for (const std::string &voxel: voxels) {
    geometry_msgs::Point point;

    std::stringstream stream(voxel);
    stream >> point.x >> point.y;
    if (stream) {
      stream >> point.z; // the z value is 0 by default
    }

    monitor.addZone(point);
  }

  std::set<ros::Subscriber> topicSubscribers;
  for (const auto& topic : safety_zone_monitor::readTopicList(n)) {
    topicSubscribers.insert(n.subscribe(n.getNamespace() + "/" + topic, 1000, &SafetyZoneMonitor::providerCallback, &monitor));
  }

  ros::MultiThreadedSpinner spinner(1);
  ros::spin(spinner);

}