//
// Created by Johannes Mey on 13.07.20.
//

#ifndef SRC_TIMEDPLANNER_H
#define SRC_TIMEDPLANNER_H


#include <boost/optional.hpp>
#include "util/TrajectoryUtil.h"

#include "std_msgs/Float64.h"
#include "panda_mqtt_connector/Trajectory.h"

using moveit::planning_interface::MoveGroupInterface;

class TimedPlanner {

public:
  const double default_velocity = 1.0;
  const std::string default_planning_mode = TrajectoryUtil::FLUID_PATH;

  static std::vector<moveit_msgs::RobotTrajectory>
  split(MoveGroupInterface &group, const MoveGroupInterface::Plan &plan);

  void doMotion();

  TimedPlanner(ros::NodeHandle &node_handle, MoveGroupInterface &group, double defaultVelocity, std::string defaultPlanningMode);

  void newTrajectoryCallback(const panda_mqtt_connector::Trajectory::ConstPtr &msg);
  void newMotionSpeedFactorCallback(const std_msgs::Float64ConstPtr &msg);

private:
  double motionSpeedFactor;

  std::shared_ptr<ros::NodeHandle> node_handle;
  std::shared_ptr<moveit::planning_interface::MoveGroupInterface> group;

  panda_mqtt_connector::Trajectory currentTrajectory;
  boost::optional<panda_mqtt_connector::Trajectory> nextTrajectory;

  bool updateWaypoints();

  void loadSquareTrajectory();

  void loadCircularTrajectory(double radius, geometry_msgs::Point origin, int granularity);
};


#endif //SRC_TIMEDPLANNER_H
